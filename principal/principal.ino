//----ESTE SKETCH CORRESPONDE AL MODULO DE CONTROL---//
//------------------MODULO ESP8266-12---------------------//

#include <ESP8266WiFi.h>
#include <EEPROM.h>
#include <Wire.h>
#include "RTClib.h"
#include <PubSubClient.h>

//----------------DECLARACION DEL RTC---------------------//
  RTC_DS1307 RTC;
  int horaInicio = 19;            // Hora de inicio de la vigilancia 
  int minInicio = 30;             // Minutos de inicio de la vigilancia 
  int horaFinal = 05;             // Hora de finalizar la vigilancia 
  int minFinal = 59;              // Minutos de finalizar la vigilancia 
  
//----------------VARIABLES Y CONSTANTES--------------------//
    
  int contconexion = 0;               // Variables de conexion con el router WIFI
  const char *ssid = "ARAWA-GUIFI";
  const char *password = "ArawatO-123456098";
  
  char SERVER[50]   = "3.83.223.148"; //"soldier.cloudmqtt.com" Datos para conexion con broker MQTT
  int SERVERPORT   = 18591;
  String USERNAME = "Arawato_Charallave";   
  char PASSWORD[50] = "12345678"; 

  int sistema = 1;                    // Variables para la activacion del sistema de alarmas 0=Apagado, 1=Encendido Siempre, 2=Horario Normal
  boolean detSensorA = false;              
  boolean detSensorB = false;
  boolean activacion = false;
  boolean reportar = false;
  
  const long intervalSir = (1*10*1000);     // Tiempo de sonido de la sirena
  unsigned long previousMillisA = 0;        // Contador de tiempo transcurrido
  const long intervalReporte = (3*1000);    // Tiempo de envio de reporte
  unsigned long previousMillisB = 0;        // Contador de tiempo transcurrido
  
  char PLACA[50];                           // Variables para el envio y recepcion de datos de MQTT
  String strSensor;
  String strSistema;                    
  char valueStr[15];
  char DETECCION_MOVIMIENTO[50];
  char MODOSISTEMA[50];
  char REPORTESISTEMA[50];
  char HORARIOSISTEMA[50];
                     
  const int pinSenA = 14;                   // Asignacion de nombres a los pines de la placa
  const int pinSenB = 12;
  const int pinSir = 2;           


//----------------------Cliente WIFI---------------------------//
WiFiClient espClient;
PubSubClient client(espClient);

//-----------------Función para leer la EEPROM------------------------
String leer(int addr) {
  byte valor;
  valor = EEPROM.read(addr);
  Serial.print("EEPROM leida en la posicion: ");
  Serial.print(addr);
  Serial.print("; Retornando valor: ");
  Serial.println(valor, DEC);
  return (String)valor;

}

//----------------Función para grabar en la EEPROM-------------------
void grabar(int addr, int val) {
  EEPROM.write(addr, val);
  EEPROM.commit();
  Serial.print("Dato grabado: ");
  Serial.print(val);
  Serial.print("; En la EEPROM, posicion: ");
  Serial.println(addr);
}
//------------------------CALLBACK-----------------------------//
void callback(char* topic, byte* payload, unsigned int length) {

  char PAYLOAD[10] = "         ";
  
  Serial.print("Mensaje Recibido: [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    PAYLOAD[i] = (char)payload[i];
  }
  Serial.println(PAYLOAD);
  
  if (String(topic) ==  String(MODOSISTEMA)) {
    if (payload[0] == '0'){
      digitalWrite(pinSir, LOW);
      sistema = 0;
      grabar(0,0);
    }
    if (payload[0] == '1'){
      sistema = 1;
      grabar(0,1);
    }
    if (payload[0] == '2'){
      sistema = 2;
      grabar(0,2);
    }
  }
  
  if (String(topic) ==  String(HORARIOSISTEMA)) {
    
    char HORAI[3] = "  ";
    char MINI[3] = "  ";
    char HORAF[3] = "  ";
    char MINF[3] = "  ";

    HORAI[0] = PAYLOAD[0];
    HORAI[1] = PAYLOAD[1];

    MINI[0] = PAYLOAD[3];
    MINI[1] = PAYLOAD[4];

    HORAF[0] = PAYLOAD[5];
    HORAF[1] = PAYLOAD[6];

    MINF[0] = PAYLOAD[8];
    MINF[1] = PAYLOAD[9];

    horaInicio = String(HORAI).toInt();
    grabar(1, horaInicio);

    minInicio = String(MINI).toInt();
    grabar(2, minInicio);
    
    horaFinal = String(HORAF).toInt();
    grabar(3, horaFinal);
    
    minFinal = String(MINF).toInt();
    grabar(4, minFinal);
    
  }
  reportar = true;
}

//-----------------------RECONNECT-----------------------------//
void reconnect() {
  uint8_t retries = 10;
  // Loop hasta que estamos conectados
  while (!client.connected()) {
    Serial.print("Intentando conexion MQTT...");
    // Crea un ID de cliente al azar
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    USERNAME.toCharArray(PLACA, 50);
    if (client.connect("", PLACA, PASSWORD)) {
    //if (client.connect(clientId.c_str(), PLACA, PASSWORD)) {
      Serial.println("conectado");
      client.subscribe(MODOSISTEMA);
      client.subscribe(HORARIOSISTEMA);
    } else {
      Serial.print("fallo, rc=");
      Serial.print(client.state());
      Serial.println(" Intentando nuevamente en 1 segundos");
      // espera 1 segundos antes de reintentar
      delay(1000);
    }
    retries--;
    if (retries == 0) {
      Serial.println("No se pudo conectar con MQTT");
      return;
    }
  }
}

//--------------------------SETUP-------------------------------//
void setup() {
   
  //Inicializacion del puerto serial
  Serial.begin(115200); Serial.println();
  
  //Inicializacion de la EEPROM
  EEPROM.begin(512);

  //***TIEMPO DE ESTABLECIMIENTO DE LOS SENSORES***//
  delay(1*1000);

  //***INIZIALIZACION DEL RTC***//
  Wire.begin();
  RTC.begin();
  //RTC.adjust(DateTime(__DATE__, __TIME__)); //ESTABLECE LA FECHA Y HORA AL RTC POR PRIMERA VEZ SOLO CALGAR ESTA LINEA UNA VEZ

  //Pin del sensor como entrada
  pinMode(pinSenA, INPUT);
  pinMode(pinSenB, INPUT);
    
  //Prepara el pin de la sirena como salida
  pinMode(pinSir, OUTPUT);
  digitalWrite(pinSir, LOW);
  Serial.println("***Pines Configurados***");
  
  // Conexión WIFI -------------------------
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED and contconexion <50) { //Cuenta hasta 50 si no se puede conectar lo cancela
    ++contconexion;
    delay(500);
    Serial.print(".");
  }
  if (contconexion <50) {
    //para usar con ip fija
    //IPAddress ip(192,168,1,156); 
    //IPAddress gateway(192,168,1,1); 
    //IPAddress subnet(255,255,255,0); 
    //WiFi.config(ip, gateway, subnet); 
      
    Serial.println("");
    Serial.println("WiFi conectado");
    Serial.println(WiFi.localIP());
  } else { 
    Serial.println("");
    Serial.println("Error de conexion al WIFI");
    delay(1);
  }
    
  // Iniciar servidor ---------------------------
  client.setServer(SERVER, SERVERPORT);
  client.setCallback(callback);
  
  // Declaracion de topics ---------------------------
  String Deteccion_Movimiento = "/" + USERNAME + "/" + "Deteccion_Movimiento"; 
  Deteccion_Movimiento.toCharArray(DETECCION_MOVIMIENTO, 50);

  String modoSistema = "/" + USERNAME + "/" + "modoSistema"; 
  modoSistema.toCharArray(MODOSISTEMA, 50);

  String reporteSistema = "/" + USERNAME + "/" + "reporteSistema"; 
  reporteSistema.toCharArray(REPORTESISTEMA, 50);

  String horarioSistema = "/" + USERNAME + "/" + "horarioSistema"; 
  horarioSistema.toCharArray(HORARIOSISTEMA, 50);

  // Verificar y activar conexion con MQTT ----------------
  if (!client.connected()) {
    reconnect();
  }
  
  //Lee la ultima conf guardada en la EEPROM ----------------
  sistema = leer(0).toInt();
  horaInicio = leer(1).toInt();          
  minInicio = leer(2).toInt();            
  horaFinal = leer(3).toInt();              
  minFinal = leer(4).toInt();  
              
  Serial.print("Ultima Conf guardada del sistema es: ");
  Serial.print(sistema);
  Serial.println(" ...Donde 0=Apagado, 1=Encendido Siempre, 2=Horario Normal");
  Serial.print("Ultimo Horario guardada del sistema es: Desde las ");
  Serial.print(horaInicio);Serial.print(":");Serial.print(minInicio);Serial.print(" Hasta las ");
  Serial.print(horaFinal);Serial.print(":");Serial.print(minFinal);Serial.println(" .");
  
  //Captura la Fecha y la Hora por el RTC -------------------
  DateTime now = RTC.now();
  Serial.print("Fecha actual: ");
  Serial.print(now.year(), DEC); Serial.print('/');
  Serial.print(now.month(), DEC); Serial.print('/');
  Serial.print(now.day(), DEC); Serial.print(' ');
  Serial.print(now.hour(), DEC); Serial.print(':');
  Serial.print(now.minute(), DEC); Serial.print(':');
  Serial.print(now.second(), DEC); Serial.print(" Dia de la semana: ");
  Serial.println(now.dayOfTheWeek(), DEC);
}

//--------------------------LOOP--------------------------------//
void loop() {

  client.loop();

  DateTime now = RTC.now();                     //Obtine la fecha y hora actual;  Dias de la semana (now.dayOfTheWeek()) => DOM=0, LUN=1, MAR=2, MIE=3, JUE=4, VIE=5, SAB=6
                                                // Condicion para el encendido del sistema 0=Apagado, 1=Encendido Siempre, 2=Horario Normal
  
  /* if ( (sistema == 1) || 
       ( (sistema == 2) && 
         ((now.hour() >= horaInicio) && (now.minute() >= minInicio)) && 
         ((now.hour() <= horaFinal) && (now.minute() < minFinal)) ) 
     ){ */

  if ( sistema == 1 ) {
  
    // Alerta del sensor de la placa principal ----------------
    if( digitalRead(pinSenA) == 0 ){
      detSensorA = true;
      strSensor = "Zona A";
    } else {
        detSensorA = false;
    }

    if( digitalRead(pinSenB) == 0 ){
      detSensorB = true;
      strSensor = "Zona B";
    } else {
        detSensorB = false;
    }
          
    // Encendido de la sirena y envio de dato a MQTT ----------
    if( (detSensorA == true || detSensorB == true) && (activacion == false) ){
    //if( detSensorA == true || detSensorB == true){
      digitalWrite(pinSir, HIGH);     // Encendido de la Sirena
      detSensorA = false;             // Bajo los estados de los Sensores
      detSensorB = false;             // Bajo los estados de los Sensores
      activacion = true;              // Deja la activacio en alto para la sirena
      previousMillisA = millis();      // Guarda el tiempo en que fue activa la sirena
      Serial.println("Alarma Activa");

      // Envio de dato al broker de MQTT
      strSensor.toCharArray(valueStr, 15);                                  
      Serial.println("Enviando: [" +  String(DETECCION_MOVIMIENTO) + "] " + strSensor);
      client.publish(DETECCION_MOVIMIENTO, valueStr);
      Serial.println("***************** Alarma Activa Publicado ********************");
    }
      
    // Apagado de la sirena -----------------------------------
    unsigned long currentMillisA = millis(); // Guarda el tiempo actual
    if ((activacion == true) && (currentMillisA - previousMillisA >= intervalSir)) { // Verificar que se haya activado la alarmar y que ya paso el intervalo
      digitalWrite(pinSir, LOW);        // Apaga la sirena
      activacion = false;               // Baja la activacion
      detSensorA = false;               // Bajo los estados de los Sensores
      detSensorB = false;               // Bajo los estados de los Sensores
    }
  }
  
  // Reporte del sistema -----------------------------------
  unsigned long currentMillisB = millis();
    
  if ( (currentMillisB - previousMillisB >= intervalReporte) || (reportar == true) ){ // Envia el reporte cada vez que se cumple el intervalo o hay un cambio en la modalidad
    previousMillisB = currentMillisB;
    reportar = false;

    if (sistema == 2) {
      
      strSistema = (String(sistema) + "-" + String(horaInicio) + ":" + String(minInicio) + "-" + String(horaFinal) + ":" + String(minFinal));
      
      strSistema.toCharArray(valueStr, 15);
      Serial.print("Enviando: [" +  String(REPORTESISTEMA) + "] " + strSistema + " ");
      client.publish(REPORTESISTEMA, valueStr);
      Serial.println("/// Reporte Publicado");
    }
    else {
      
      strSistema = String(sistema);
      
      strSistema.toCharArray(valueStr, 15);
      Serial.print("Enviando: [" +  String(REPORTESISTEMA) + "] " + strSistema + " ");
      client.publish(REPORTESISTEMA, valueStr);
      Serial.println("/// Reporte Publicado");
    }
  }
}
